use std::collections::{HashMap, VecDeque};
use std::io::ErrorKind;
use std::net::{SocketAddr, TcpListener};
use std::sync::Arc;

use rustls::{ServerConfig, ServerConnection, StreamOwned};
use rustls::server::ServerConnectionData;
use switch_protocol::{ClientConnectionStatus, ConnectionStatus, ServerEvent, ServerPacket, SwitchError};
use switch_protocol::Client;
use switch_protocol::internal::Server;

use crate::TlsClient;

pub struct TlsServer {
    listener: TcpListener,
    events: VecDeque<ServerEvent>,
    clients: HashMap<String, TlsClient<ServerConnection, ServerConnectionData>>,
    config: Arc<ServerConfig>,
}

impl TlsServer {
    pub fn new(listener: TcpListener, config: ServerConfig) -> Self {
        listener.set_nonblocking(true).unwrap();
        Self {
            listener,
            events: VecDeque::new(),
            clients: HashMap::new(),
            config: Arc::new(config),
        }
    }
}

impl Server for TlsServer {
    type Address = SocketAddr;

    fn update(&mut self) -> Result<(), SwitchError> {
        let res = self.listener.accept();
        match res {
            Err(e) => {
                if e.kind() != ErrorKind::WouldBlock {
                    return Err(SwitchError::OsError { id: e.raw_os_error().unwrap_or(-1) });
                }
            }
            Ok((sock, _)) => {
                let id = uuid::Uuid::new_v4().to_string();
                let conn = ServerConnection::new(self.config.clone()).unwrap();
                let stream = StreamOwned::new(conn, sock);
                let client = TlsClient::new(stream);
                self.events.push_back(ServerEvent::ClientConnectionStatus(ClientConnectionStatus::new(&id, ConnectionStatus::Connected)));
                self.clients.insert(id, client);
            }
        }
        let mut to_be_removed = Vec::new();
        for (id, client) in self.clients.iter_mut() {
            let res = client.update();
            match res {
                Err(_) => {
                    to_be_removed.push(id.to_string());
                    continue;
                }
                Ok(_) => {}
            }
            while let Some(packet) = client.poll() {
                self.events.push_back(ServerEvent::ReceivedPacket(ServerPacket::new(id, packet)));
            }
        }
        for id in to_be_removed.iter() {
            self.clients.remove(id).unwrap();
            self.events.push_back(ServerEvent::ClientConnectionStatus(ClientConnectionStatus::new(id, ConnectionStatus::Disconnected)));
        }
        Ok(())
    }

    fn poll(&mut self) -> Option<ServerEvent> {
        self.events.pop_front()
    }

    fn close_client(&mut self, id: &str) {
        self.clients.remove(id);
    }

    fn send(&mut self, id: &str, packet: Vec<u8>) -> Result<(), SwitchError> {
        if let Some(client) = self.clients.get_mut(id) {
            return client.send(packet);
        }
        Err(SwitchError::NoClientWithId { id: id.to_string() })
    }

    fn client_peer_addr(&self, id: &str) -> Result<Self::Address, SwitchError> {
        match self.clients.get(id) {
            None => { Err(SwitchError::NoClientWithId { id: id.to_string() }) }
            Some(client) => { client.peer_addr() }
        }
    }

    fn client_local_addr(&self, id: &str) -> Result<Self::Address, SwitchError> {
        match self.clients.get(id) {
            None => { Err(SwitchError::NoClientWithId { id: id.to_string() }) }
            Some(client) => { client.local_addr() }
        }
    }
}