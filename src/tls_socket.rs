use std::io::{ErrorKind, Read, Write};
use std::net::{SocketAddr, TcpStream};
use std::ops::{Deref, DerefMut};

use rustls::{ConnectionCommon, SideData, StreamOwned};
use switch_protocol::internal::Socket;
use switch_protocol::SwitchError;

pub struct TlsSocket<T> {
    conn: StreamOwned<T, TcpStream>,
}

impl<T> TlsSocket<T> {
    pub fn new(conn: StreamOwned<T, TcpStream>) -> Self {
        Self { conn }
    }
}

impl<T: DerefMut<Target=ConnectionCommon<D>>, D: SideData> Socket for TlsSocket<T> {
    type Address = SocketAddr;

    fn read(&mut self, buf: &mut [u8]) -> Result<usize, SwitchError> {
        match self.conn.conn.reader().read(buf) {
            Ok(size) => { Ok(size) }
            Err(err) => {
                if err.kind() != ErrorKind::WouldBlock {
                    Err(SwitchError::OsError { id: err.raw_os_error().unwrap_or(-1) })
                } else {
                    Ok(0)
                }
            }
        }
    }

    fn write(&mut self, buf: &[u8]) -> Result<usize, SwitchError> {
        match self.conn.conn.writer().write(buf) {
            Ok(size) => { Ok(size) }
            Err(err) => {
                if err.kind() != ErrorKind::WouldBlock {
                    Err(SwitchError::OsError { id: err.raw_os_error().unwrap_or(-1) })
                } else {
                    Ok(0)
                }
            }
        }
    }

    fn set_nonblocking(&self, nonblocking: bool) -> Result<(), SwitchError> {
        match self.conn.get_ref().set_nonblocking(nonblocking) {
            Ok(_) => { Ok(()) }
            Err(err) => { Err(SwitchError::OsError { id: err.raw_os_error().unwrap_or(-1) }) }
        }
    }

    fn local_addr(&self) -> Result<Self::Address, SwitchError> {
        match self.conn.get_ref().local_addr() {
            Ok(addr) => { Ok(addr) }
            Err(err) => { Err(SwitchError::OsError { id: err.raw_os_error().unwrap_or(-1) }) }
        }
    }

    fn peer_addr(&self) -> Result<Self::Address, SwitchError> {
        match self.conn.get_ref().peer_addr() {
            Ok(addr) => { Ok(addr) }
            Err(err) => { Err(SwitchError::OsError { id: err.raw_os_error().unwrap_or(-1) }) }
        }
    }
}

impl<T: DerefMut<Target=ConnectionCommon<D>>, D: SideData> Deref for TlsSocket<T> {
    type Target = StreamOwned<T, TcpStream>;

    fn deref(&self) -> &Self::Target {
        &self.conn
    }
}

impl<T: DerefMut<Target=ConnectionCommon<D>>, D: SideData> DerefMut for TlsSocket<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.conn
    }
}