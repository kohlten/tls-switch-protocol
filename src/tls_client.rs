use std::collections::VecDeque;
use std::io::ErrorKind;
use std::net::{SocketAddr, TcpStream};
use std::ops::DerefMut;

use rustls::{ConnectionCommon, SideData, StreamOwned};
use switch_protocol::{Client, SwitchError};
use switch_protocol::internal::Protocol;

use crate::TlsSocket;

pub struct TlsClient<T: DerefMut<Target=ConnectionCommon<D>>, D: SideData> {
    conn: Protocol<TlsSocket<T>, SocketAddr>,
    events: VecDeque<Vec<u8>>,
}

impl<T: DerefMut<Target=ConnectionCommon<D>>, D: SideData> TlsClient<T, D> {
    pub fn new(conn: StreamOwned<T, TcpStream>) -> Self {
        Self { conn: Protocol::new(TlsSocket::new(conn)), events: VecDeque::new() }
    }
}

impl<T: DerefMut<Target=ConnectionCommon<D>>, D: SideData> Client for TlsClient<T, D> {
    type Address = SocketAddr;

    fn update(&mut self) -> Result<(), SwitchError> {
        let stream = self.conn.deref_mut().deref_mut();
        match stream.conn.complete_io(&mut stream.sock) {
            Ok(_) => {}
            Err(err) => {
                if err.kind() != ErrorKind::WouldBlock {
                    return Err(SwitchError::OsError { id: err.raw_os_error().unwrap_or(-1) });
                }
            }
        }
        let res = self.conn.update();
        match res {
            Err(e) => {
                return Err(e);
            }
            Ok(_) => {}
        }
        while let Some(packet) = self.conn.poll() {
            self.events.push_front(packet);
        }
        Ok(())
    }

    fn send(&mut self, packet: Vec<u8>) -> Result<(), SwitchError> {
        self.conn.send(packet)
    }

    fn poll(&mut self) -> Option<Vec<u8>> {
        self.events.pop_back()
    }

    fn peer_addr(&self) -> Result<Self::Address, SwitchError> {
        self.conn.peer_addr()
    }

    fn local_addr(&self) -> Result<Self::Address, SwitchError> {
        self.conn.local_addr()
    }
}